package Assignment3.Assignment3;

import Presentation.*;

public class Main {

	public static void main(String[] args) {
		MainView v = new MainView();
		v.setVisible(true);
		Controller c = new Controller(v);
	}
}
